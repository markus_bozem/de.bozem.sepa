package de.bozem.sepa;

import org.adempiere.base.IBankStatementLoaderFactory;
import org.adempiere.base.IPaymentExporterFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		SEPABankStatementLoaderFactory sepaBankStatementLoaderFactory = new SEPABankStatementLoaderFactory() ;
		context.registerService(IBankStatementLoaderFactory.class.getName(), sepaBankStatementLoaderFactory, null) ;
		SEPAPaymentExporterFactory sepaPaymentExporterFactory = new SEPAPaymentExporterFactory() ;
		context.registerService(IPaymentExporterFactory.class.getName(), sepaPaymentExporterFactory, null) ;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
