package de.bozem.sepa;

import java.util.List;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.compiere.model.MInvoice;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.osgi.service.event.Event;

public class MInvoiceEventHandler extends AbstractEventHandler {

	CLogger log = CLogger.getCLogger(MInvoiceEventHandler.class);
	int reverseId = 0;

	@Override
	protected void initialize() {
		registerTableEvent(IEventTopics.DOC_BEFORE_COMPLETE, MInvoice.Table_Name);
		registerTableEvent(IEventTopics.DOC_BEFORE_REVERSECORRECT, MInvoice.Table_Name);
		registerTableEvent(IEventTopics.DOC_BEFORE_REVERSEACCRUAL, MInvoice.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_REVERSECORRECT, MInvoice.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_REVERSEACCRUAL, MInvoice.Table_Name);
	}

	@Override
	protected void doHandleEvent(Event event) {
		PO po = getPO(event);
		String topic = event.getTopic();
		if (po instanceof MInvoice) {
			MInvoice invoice = (MInvoice) po;

			if (event.getTopic().equals(IEventTopics.DOC_BEFORE_REVERSECORRECT)
					|| event.getTopic().equals(IEventTopics.DOC_BEFORE_REVERSEACCRUAL)) {
				reverseId = po.get_ID();
			}

			if (event.getTopic().equals(IEventTopics.DOC_AFTER_REVERSECORRECT)
					|| event.getTopic().equals(IEventTopics.DOC_AFTER_REVERSEACCRUAL)) {
				reverseId = 0;
			}

			if (event.getTopic().equals(IEventTopics.DOC_BEFORE_COMPLETE)) {

				log.warning("----------------MInvoice check!!!!");
				String POReference = (String) invoice.get_Value ("POReference") ;
				String externalDocumentNo = (String) invoice.get_Value("ExternalDocumentNo") ;;
				if ((externalDocumentNo == null || externalDocumentNo.length() == 0) && (POReference == null || POReference.length() == 0)) {
					throw new RuntimeException("Externe Belegnummer oder Partner Referenz muß ausgefüllt sein!");
				} else {
					int bpid = invoice.getC_BPartner_ID();

					MTable table = MTable.get(Env.getCtx(), MInvoice.Table_ID);
					// String where = "IsActive='Y' and (DocStatus='CO' or DocStatus='DR' or
					// DocStatus='IN') and c_bpartner_id=" + bpid + " and poreference='" + poref
					// +"'";
					String where = "IsActive='Y' and (DocStatus='CO') and c_bpartner_id=" + bpid 
							+ " and not c_invoice_id=" + po.get_ID() +" and (";
					if (externalDocumentNo!=null && externalDocumentNo.length()>0)
					{
						where += "externaldocumentno='"+ externalDocumentNo + "'"
							+ " or poreference='"+ externalDocumentNo + "'" ;
					}
					if (externalDocumentNo!=null && externalDocumentNo.length()>0 && POReference!=null && POReference.length()>0)
					{
						where += " or " ;
					}
					if (POReference!=null && POReference.length()>0)
					{
						where += "externaldocumentno='"+ POReference + "'"
							+ " or poreference='"+ POReference + "'" ;
					}
					where += ")" ;
					if (reverseId > 0) {
						where = where + " and not c_invoice_id=" + reverseId;
					}
					Query query = table.createQuery(where, null);
					List<PO> list = query.list();
					if (list.size() > 0) {
						throw new RuntimeException("Externe Belegnummer oder Partner Referenz wurde schon erfaßt!");
					}
				}

			}
		}
	}

}
