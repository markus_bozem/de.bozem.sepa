/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package de.bozem.sepa;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.compiere.model.I_C_BankAccount;
import org.compiere.model.I_C_Invoice;
import org.compiere.model.I_C_PaySelection;
import org.compiere.model.MBPBankAccount;
import org.compiere.model.MBPartner;
import org.compiere.model.MClient;
import org.compiere.model.MCurrency;
import org.compiere.model.MInvoice;
import org.compiere.model.MPaySelectionCheck;
import org.compiere.model.MPaySelectionLine;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.IBAN;
import org.compiere.util.PaymentExport;
import org.compiere.util.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * SEPA Payment Export based on generic export example
 * 
 * @author integratio/pb
 * @author mbozem@bozem.de
 * 
 */

public class SEPAPaymentExporter implements PaymentExport {
	/** Logger */
	static private CLogger s_log = CLogger.getCLogger(SEPAPaymentExporter.class);
	private static String DocumentType = "pain.001.002.03";

	/**************************************************************************
	 * Export to File
	 * 
	 * @param checks
	 *            array of checks
	 * @param file
	 *            file to export checks
	 * @return number of lines
	 */
	@Override
	public int exportToFile(MPaySelectionCheck[] checks, boolean depositBatch, String paymentRule, File file,
			StringBuffer err) {

		int noLines = 0;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbf.newDocumentBuilder();
			Document document = builder.newDocument();

			MClient adc = new MClient(Env.getCtx(), Env.getAD_Client_ID(Env.getCtx()), null);

			String MsgId;
			String CreationDate;
			int NumberOfTransactions = 0;
			String InitiatorName = adc.getName();
			String PaymentInfoId = "Payments";
			BigDecimal CtrlSum;
			String ExecutionDate;
			String Dbtr_Name = adc.getName();
			String DbtrAcct_IBAN;
			String DbtrAcct_BIC;

			CtrlSum = BigDecimal.ZERO;

			for (int i = 0; i < checks.length; i++) {
				MPaySelectionCheck mpp = checks[i];
				CtrlSum = CtrlSum.add(mpp.getPayAmt());
				NumberOfTransactions++;
			}

			MPaySelectionCheck mppC = checks[0];
			I_C_PaySelection ps = mppC.getC_PaySelection();

			MsgId = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(ps.getCreated());

			ExecutionDate = new SimpleDateFormat("yyyy-MM-dd").format(ps.getPayDate());
			CreationDate = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis()) + "T"
					+ new SimpleDateFormat("HH:mm:ss").format(System.currentTimeMillis()) + ".000Z";

			I_C_BankAccount cba = ps.getC_BankAccount();
			DbtrAcct_IBAN = IBAN.normalizeIBAN(cba.getIBAN());
			DbtrAcct_BIC = cba.getC_Bank().getSwiftCode();

			if (!IBAN.isValid(DbtrAcct_IBAN)) {
				err.append("IBAN " + DbtrAcct_IBAN + " is not valid.");
				return -1;
			}

			if (!Util.isEmpty(DbtrAcct_BIC) && DbtrAcct_BIC.length() > 11) {
				err.append("BIC/SWIFTCode " + DbtrAcct_BIC + " is not valid.");
				return -1;
			}

			Element root = document.createElement("Document");
			root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
			root.setAttribute("xmlns", "urn:iso:std:iso:20022:tech:xsd:" + DocumentType);

			Element CstmrCdtTrfInitnElement = document.createElement("CstmrCdtTrfInitn"); // Begin
																							// of
																							// CstmrCdtTrfInitnElement

			Element GrpHdrElement = document.createElement("GrpHdr");
			GrpHdrElement.appendChild(document.createElement("MsgId")).setTextContent(iSEPA_ConvertSign(MsgId, 35));
			GrpHdrElement.appendChild(document.createElement("CreDtTm"))
					.setTextContent(iSEPA_ConvertSign(CreationDate));
			GrpHdrElement.appendChild(document.createElement("NbOfTxs"))
					.setTextContent(String.valueOf(NumberOfTransactions));
			GrpHdrElement.appendChild(document.createElement("InitgPty")).appendChild(document.createElement("Nm"))
					.setTextContent(iSEPA_ConvertSign(InitiatorName, 70));
			CstmrCdtTrfInitnElement.appendChild(GrpHdrElement);

			Element PmtInfElement = null;
			
			if (depositBatch) {

				PmtInfElement = document.createElement("PmtInf"); // Begin
																			// of
																			// PmtInf

				PmtInfElement.appendChild(document.createElement("PmtInfId"))
						.setTextContent(iSEPA_ConvertSign(PaymentInfoId, 35));
				PmtInfElement.appendChild(document.createElement("PmtMtd")).setTextContent("TRF");

				PmtInfElement.appendChild(document.createElement("BtchBookg")).setTextContent("true");

				PmtInfElement.appendChild(document.createElement("NbOfTxs"))
						.setTextContent(String.valueOf(NumberOfTransactions));
				PmtInfElement.appendChild(document.createElement("CtrlSum")).setTextContent(String.valueOf(CtrlSum));
				PmtInfElement.appendChild(document.createElement("PmtTpInf"))
						.appendChild(document.createElement("SvcLvl")).appendChild(document.createElement("Cd"))
						.setTextContent("SEPA");
				PmtInfElement.appendChild(document.createElement("ReqdExctnDt"))
						.setTextContent(iSEPA_ConvertSign(ExecutionDate));
				PmtInfElement.appendChild(document.createElement("Dbtr")).appendChild(document.createElement("Nm"))
						.setTextContent(iSEPA_ConvertSign(Dbtr_Name, 70));
				PmtInfElement.appendChild(document.createElement("DbtrAcct")).appendChild(document.createElement("Id"))
						.appendChild(document.createElement("IBAN")).setTextContent(DbtrAcct_IBAN);
				PmtInfElement.appendChild(document.createElement("DbtrAgt"))
						.appendChild(document.createElement("FinInstnId")).appendChild(document.createElement("BIC"))
						.setTextContent(iSEPA_ConvertSign(DbtrAcct_BIC));
				PmtInfElement.appendChild(document.createElement("ChrgBr")).setTextContent("SLEV");
			}

			for (int i = 0; i < checks.length; i++) {
				MPaySelectionCheck mpp = checks[i];

				if (mpp == null)
					continue;
				mpp.getC_BPartner_ID();
				mpp.getDocumentNo();
				mpp.isProcessed();
				String PmtId = String.valueOf(adc.getAD_Client_ID()) + "-" + String.valueOf(mpp.get_ID());
				String CreditorName;
				String CdtrAcct_BIC;
				String CdtrAcct_IBAN;
				String unverifiedReferenceLine;

				unverifiedReferenceLine = getUnverifiedReferenceLine(mpp);

				MBPartner bPartner = new MBPartner(Env.getCtx(), mpp.getC_BPartner_ID(), null);
				CreditorName = bPartner.getName();

				MBPBankAccount bpBankAccount = getBPartnerAccount(bPartner);
				if (bpBankAccount == null) {
					err.append("No bankaccount. Creditor: " + CreditorName);
					return -1;
				}
				CdtrAcct_IBAN = IBAN.normalizeIBAN(bpBankAccount.getIBAN());
				CdtrAcct_BIC = bpBankAccount.getSwiftCode();

				if (!IBAN.isValid(CdtrAcct_IBAN)) {
					err.append("IBAN " + CdtrAcct_IBAN + " is not valid. Creditor: " + CreditorName);
					return -1;
				}

				if (!depositBatch) {

					PmtInfElement = document.createElement("PmtInf"); // Begin
					// of
					// PmtInf

					PmtInfElement.appendChild(document.createElement("PmtInfId"))
							.setTextContent(iSEPA_ConvertSign(PaymentInfoId, 35));
					PmtInfElement.appendChild(document.createElement("PmtMtd")).setTextContent("TRF");

					PmtInfElement.appendChild(document.createElement("BtchBookg")).setTextContent("false");

					PmtInfElement.appendChild(document.createElement("NbOfTxs"))
							.setTextContent(String.valueOf(1));
					PmtInfElement.appendChild(document.createElement("CtrlSum"))
							.setTextContent(String.valueOf(mpp.getPayAmt()));
					PmtInfElement.appendChild(document.createElement("PmtTpInf"))
							.appendChild(document.createElement("SvcLvl")).appendChild(document.createElement("Cd"))
							.setTextContent("SEPA");
					PmtInfElement.appendChild(document.createElement("ReqdExctnDt"))
							.setTextContent(iSEPA_ConvertSign(ExecutionDate));
					PmtInfElement.appendChild(document.createElement("Dbtr")).appendChild(document.createElement("Nm"))
							.setTextContent(iSEPA_ConvertSign(Dbtr_Name, 70));
					PmtInfElement.appendChild(document.createElement("DbtrAcct"))
							.appendChild(document.createElement("Id")).appendChild(document.createElement("IBAN"))
							.setTextContent(DbtrAcct_IBAN);
					PmtInfElement.appendChild(document.createElement("DbtrAgt"))
							.appendChild(document.createElement("FinInstnId"))
							.appendChild(document.createElement("BIC")).setTextContent(iSEPA_ConvertSign(DbtrAcct_BIC));
					PmtInfElement.appendChild(document.createElement("ChrgBr")).setTextContent("SLEV");

				}

				Element CdtTrfTxInfElement = document.createElement("CdtTrfTxInf");

				CdtTrfTxInfElement.appendChild(document.createElement("PmtId"))
						.appendChild(document.createElement("EndToEndId")).setTextContent(iSEPA_ConvertSign(PmtId, 35));

				Element InstdAmtElement = document.createElement("InstdAmt");
				InstdAmtElement.setAttribute("Ccy",
						MCurrency.getISO_Code(Env.getCtx(), mpp.getParent().getC_Currency_ID()));
				InstdAmtElement.setTextContent(String.valueOf(mpp.getPayAmt()));
				CdtTrfTxInfElement.appendChild(document.createElement("Amt")).appendChild(InstdAmtElement);
				CdtTrfTxInfElement.appendChild(document.createElement("CdtrAgt"))
						.appendChild(document.createElement("FinInstnId")).appendChild(document.createElement("BIC"))
						.setTextContent(iSEPA_ConvertSign(CdtrAcct_BIC));
				CdtTrfTxInfElement.appendChild(document.createElement("Cdtr")).appendChild(document.createElement("Nm"))
						.setTextContent(iSEPA_ConvertSign(CreditorName, 70));
				CdtTrfTxInfElement.appendChild(document.createElement("CdtrAcct"))
						.appendChild(document.createElement("Id")).appendChild(document.createElement("IBAN"))
						.setTextContent(CdtrAcct_IBAN);
				CdtTrfTxInfElement.appendChild(document.createElement("RmtInf"))
						.appendChild(document.createElement("Ustrd"))
						.setTextContent(iSEPA_ConvertSign(unverifiedReferenceLine, 140));
				PmtInfElement.appendChild(CdtTrfTxInfElement);

				if (!depositBatch) {
					CstmrCdtTrfInitnElement.appendChild(PmtInfElement);
				}

			}

			if (depositBatch) {
				CstmrCdtTrfInitnElement.appendChild(PmtInfElement);
			}

			root.appendChild(CstmrCdtTrfInitnElement);
			document.appendChild(root);

			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(file);
			TransformerFactory tf = TransformerFactory.newInstance();

			Transformer serializer = tf.newTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.transform(domSource, streamResult);

			noLines = NumberOfTransactions;
		} catch (Exception e) {
			err.append(e.toString());
			s_log.log(Level.SEVERE, "", e);
			return -1;
		}

		return noLines;
	} // exportToFile

	/**
	 * 
	 * Generate unstructured reference line
	 * 
	 * @param mpp
	 *            check
	 * @return String with the reference line
	 * 
	 *         see EACT www.eact.eu/main.php?page=SEPA
	 */

	private String getUnverifiedReferenceLine(MPaySelectionCheck mpp) {
		MPaySelectionLine[] mPaySelectionLines = mpp.getPaySelectionLines(true);

		StringBuffer remittanceInformationSB = new StringBuffer();

		for (MPaySelectionLine mPaySelectionLine : mPaySelectionLines) {
			String documentNo = null;
			I_C_Invoice invoice = mPaySelectionLine.getC_Invoice();
			if (invoice != null) {
				if (remittanceInformationSB.length() == 0) {
					String referenceNo = invoice.getC_BPartner().getReferenceNo();
					if (!Util.isEmpty(referenceNo)) {
						remittanceInformationSB.append("/CNR/");
						remittanceInformationSB.append(referenceNo);
					}
				}
				documentNo = (String) ((MInvoice) invoice).get_Value("ExternalDocumentNo");
				if (documentNo == null || documentNo.length() == 0) {
					documentNo = invoice.getPOReference();
				}
				if (documentNo == null || documentNo.length() == 0) {
					documentNo = invoice.getDocumentNo();
				}
				if (documentNo != null && documentNo.length() > 0) {
					remittanceInformationSB.append("/DOC/");
					remittanceInformationSB.append(documentNo);
					if (mPaySelectionLine.getDiscountAmt().doubleValue() <= -0.01) {
						remittanceInformationSB.append("/ ");
						remittanceInformationSB.append(mPaySelectionLine.getPayAmt());
					}
				}
			}
		}

		return remittanceInformationSB.toString();
	} // getUnverifiedReferenceLine

	/**
	 * Get Vendor/Customer Bank Account Information Based on BP_
	 * 
	 * @param bPartner
	 *            BPartner
	 * @return Account of business partner
	 */

	private static MBPBankAccount getBPartnerAccount(MBPartner bPartner) {

		MBPBankAccount[] bpBankAccounts = bPartner.getBankAccounts(true);
		MBPBankAccount bpBankAccount = null;
		for (MBPBankAccount bpBankAccountTemp : bpBankAccounts) {
			if (bpBankAccountTemp.isActive() && !Util.isEmpty(bpBankAccountTemp.getIBAN())
					&& bpBankAccountTemp.isDirectDeposit()) {
				bpBankAccount = bpBankAccountTemp;
			}
		}
		return bpBankAccount;
	} // getBPartnerAccount

	private static String iSEPA_ConvertToSEPASign(String convertit) {
		String sepaconvert = MSysConfig.getValue("iSEPA_SPECIAL_SIGN", Env.getAD_Client_ID(Env.getCtx()));
		String[] TokenList = sepaconvert.split(",");

		for (int ii = 0; ii < TokenList.length; ii++) {
			int tlength = TokenList[ii].length();
			if (tlength > 2 && TokenList[ii].substring(1, 2).equals(":")) {
				if (convertit.equals(TokenList[ii].substring(0, 1))) {
					return TokenList[ii].substring(2, TokenList[ii].length());
				}
			}
		}
		return ".";
	}

	public static String iSEPA_ConvertSign(String text) {
		String sepa_characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/?:().,'+- ";
		int l = text.length();
		StringBuffer targettext = new StringBuffer();

		for (int i = 0; i < l; i++) {
			if (sepa_characters.contains(text.substring(i, i + 1))) {
				targettext.append(text.substring(i, i + 1));
			} else {
				targettext.append(iSEPA_ConvertToSEPASign(text.substring(i, i + 1)));
			}
		}

		return targettext.toString();
	}

	public static String iSEPA_ConvertSign(String text, int maxLength) {
		String targettext = iSEPA_ConvertSign(text);

		if (targettext.length() <= maxLength) {
			return targettext;
		} else {
			return targettext.substring(0, maxLength);
		}
	}

	public static void SetDocumentType(String dt) {
		DocumentType = dt;
	}

	@Override
	public String getFilenamePrefix() {
		String CreationDate = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(System.currentTimeMillis());
		return "SEPA-Credit-Transfer-" + CreationDate;
	}

	@Override
	public String getFilenameSuffix() {
		return ".xml";
	}

	@Override
	public String getContentType() {
		return "text/xml";
	}

	@Override
	public boolean supportsSeparateBooking() {
		return true;
	}

	@Override
	public boolean supportsDepositBatch() {
		return true;
	}

	@Override
	public boolean getDefaultDepositBatch() {
		return false;
	}

} // PaymentExport
