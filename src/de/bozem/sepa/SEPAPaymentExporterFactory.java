package de.bozem.sepa;

import org.adempiere.base.IPaymentExporterFactory;
import org.compiere.util.PaymentExport;

public class SEPAPaymentExporterFactory extends Object implements IPaymentExporterFactory {

	@Override
	public PaymentExport newPaymentExporterInstance(String className) {
		if (SEPAPaymentExporter.class.getName().equals(className))
			return new SEPAPaymentExporter();
		return null;
	}

}
