package de.bozem.sepa;

import org.adempiere.base.IBankStatementLoaderFactory;
import org.compiere.impexp.BankStatementLoaderInterface;

public class SEPABankStatementLoaderFactory extends Object implements IBankStatementLoaderFactory {

	@Override
	public BankStatementLoaderInterface newBankStatementLoaderInstance(String className) {
		if (SEPABankStatementLoader.class.getName().equals(className))
			return new SEPABankStatementLoader();

		return null;
	}

}
