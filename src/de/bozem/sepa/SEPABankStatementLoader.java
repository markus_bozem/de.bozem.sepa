package de.bozem.sepa;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.compiere.impexp.BankStatementLoaderInterface;
import org.compiere.model.I_C_BankAccount;
import org.compiere.model.MBankStatementLoader;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SEPABankStatementLoader implements BankStatementLoaderInterface {

	MBankStatementLoader controller;
	I_C_BankAccount bankAccount;
	File[] files;
	File doneDirectory;
	private String amt;
	private String cdtDbtInd;
	private String bookgDt;
	private String valDt;
	private String cdtrNm;
	private String cdtrAcctIBAN;
	private String dbtrNm;
	private String dbtrAcctIBAN;
	private StringBuilder ustrd = new StringBuilder();
	private String myAcct;
	private String myBic;
	private String ccy;
	private String lastError;
	private String lastErrorDescription;
	private String addtlNtryInf;

	public static CLogger log = CLogger.getCLogger(SEPABankStatementLoader.class);

	@Override
	public boolean init(MBankStatementLoader controller) {
		this.controller = controller;

		String sepa_bank_statement_loader_directory = controller.getFileName();
		if (Util.isEmpty(sepa_bank_statement_loader_directory)) {
			log.log(Level.SEVERE, "No directory for bank statements defined.");
			return false;
		}
		File directory = new File(sepa_bank_statement_loader_directory);
		if (!directory.isDirectory()) {
			log.log(Level.SEVERE, directory.getAbsolutePath() + " is not a directory.");
			return false;
		}
		doneDirectory = new File(directory, "done");
		if (!doneDirectory.exists()) {
			if (!doneDirectory.mkdirs()) {
				log.log(Level.SEVERE, "Could not create directory for imported files");
				return false;
			}
		}
		files = directory.listFiles();

		return true;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public boolean loadLines() {
		try {
			for (File file : files) {
				if (file.isFile() && file.getName().toLowerCase().endsWith(".xml")) {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(file);

					doc.getDocumentElement().normalize();

					Element stmt = (Element) doc.getElementsByTagName("Stmt").item(0);
					Element acct = (Element) stmt.getElementsByTagName("Acct").item(0);

					myAcct = acct.getElementsByTagName("IBAN").item(0).getTextContent();
					myBic = acct.getElementsByTagName("BIC").item(0).getTextContent();

					NodeList nList = doc.getElementsByTagName("Ntry");

					for (int i = 0; i < nList.getLength(); i++) {
						ustrd.setLength(0);
						amt = null;
						ccy = null;
						cdtDbtInd = null;
						bookgDt = null;
						valDt = null;
						cdtrNm = null;
						cdtrAcctIBAN = null;
						dbtrNm = null;
						dbtrAcctIBAN = null;
						addtlNtryInf = null ;
						lastError = null;
						lastErrorDescription = null;

						Node node = nList.item(i);
						if (node.getNodeType() == Node.ELEMENT_NODE) {

							Element ntryElement = (Element) node;

							amt = ntryElement.getElementsByTagName("Amt").item(0).getTextContent();
							ccy = ntryElement.getElementsByTagName("Amt").item(0).getAttributes().getNamedItem("Ccy").getTextContent() ;
							cdtDbtInd = ntryElement.getElementsByTagName("CdtDbtInd").item(0).getTextContent();
							bookgDt = ((Element) ntryElement.getElementsByTagName("BookgDt").item(0))
									.getElementsByTagName("Dt").item(0).getTextContent();
							valDt = ((Element) ntryElement.getElementsByTagName("ValDt").item(0))
									.getElementsByTagName("Dt").item(0).getTextContent();
							Element rltdPtiesElement = (Element) ntryElement.getElementsByTagName("RltdPties").item(0);

							NodeList nl ;
							if (rltdPtiesElement != null) {
								Element cdtr = (Element) rltdPtiesElement.getElementsByTagName("Cdtr").item(0);

								if (cdtr != null) {
									if ((nl = cdtr.getElementsByTagName("Nm")) != null) {
										cdtrNm = nl.item(0).getTextContent();
									}
								}

								Element cdtrAcct = (Element) rltdPtiesElement.getElementsByTagName("CdtrAcct").item(0);

								if (cdtrAcct != null) {
									cdtrAcctIBAN = ((Element) cdtrAcct.getElementsByTagName("Id").item(0))
											.getElementsByTagName("IBAN").item(0).getTextContent();
								}
								
								Element dbtr = (Element) rltdPtiesElement.getElementsByTagName("Dbtr").item(0);

								if (dbtr != null) {
									if ((nl = dbtr.getElementsByTagName("Nm")) != null) {
										dbtrNm = nl.item(0).getTextContent();
									}
								}

								Element dbtrAcct = (Element) rltdPtiesElement.getElementsByTagName("DbtrAcct").item(0);

								if (dbtrAcct != null) {
									dbtrAcctIBAN = ((Element) dbtrAcct.getElementsByTagName("Id").item(0))
											.getElementsByTagName("IBAN").item(0).getTextContent();
								}
								
							}

							Element rmtInf = ((Element) ntryElement.getElementsByTagName("RmtInf").item(0));

							NodeList ustrdList = rmtInf.getElementsByTagName("Ustrd");

							for (int ustrdI = 0; ustrdI < ustrdList.getLength(); ustrdI++) {
								Node ustrdNode = ustrdList.item(ustrdI);
								ustrd.append(ustrdNode.getTextContent());
							}
							
							addtlNtryInf = ntryElement.getElementsByTagName("AddtlNtryInf").item(0).getTextContent();

							controller.saveLine();
						}

					}

					File destinationFile = new File(doneDirectory, file.getName());
					Files.move(file.getAbsoluteFile().toPath(), destinationFile.toPath(),
							StandardCopyOption.REPLACE_EXISTING);
				}
			}
			return true;

		} catch (IOException e) {

			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String getLastErrorMessage() {
		return lastError;
	}

	@Override
	public String getLastErrorDescription() {
		return lastErrorDescription;
	}

	@Override
	public Timestamp getDateLastRun() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRoutingNo() {
		return myBic;
	}

	@Override
	public String getBankAccountNo() {
		return null;
	}

	@Override
	public String getStatementReference() {
		return null;
	}

	@Override
	public Timestamp getStatementDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
		LocalDate date = LocalDate.parse(bookgDt, formatter);
		Timestamp ts = Timestamp.valueOf(date.atStartOfDay());
		return ts;
	}

	@Override
	public String getTrxID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReference() {
		return null;
	}

	@Override
	public String getCheckNo() {
		return null;
	}

	@Override
	public String getPayeeName() {
		if (dbtrNm!=null)
		{
			return dbtrNm;
		}
		return cdtrNm;
	}

	@Override
	public String getPayeeAccountNo() {
		if (dbtrAcctIBAN!=null)
		{
			return dbtrAcctIBAN ;
		}
		return cdtrAcctIBAN;
	}

	@Override
	public Timestamp getStatementLineDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
		LocalDate date = LocalDate.parse(bookgDt, formatter);
		Timestamp ts = Timestamp.valueOf(date.atStartOfDay());
		return ts;
	}

	@Override
	public Timestamp getValutaDate() {

		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
		LocalDate date = LocalDate.parse(valDt, formatter);
		Timestamp ts = Timestamp.valueOf(date.atStartOfDay());
		return ts;
	}

	@Override
	public String getTrxType() {
		if (addtlNtryInf!=null)
		{
			return addtlNtryInf ;
		}
		return cdtDbtInd;
	}

	@Override
	public boolean getIsReversal() {
		return false;
	}

	@Override
	public String getCurrency() {
		return ccy;
	}

	@Override
	public BigDecimal getStmtAmt() {
		if (cdtDbtInd.equalsIgnoreCase("DBIT"))
		{
			return new BigDecimal(amt).negate();
		}
		if (cdtDbtInd.equalsIgnoreCase("CRDT"))
		{
			return new BigDecimal(amt);
		}
		lastError = "No known CdtDbtInd-Value" ;
		lastErrorDescription = "There was a unknown value for the transaction type in the SEPA file." ;
		return null ;
		
	}

	@Override
	public BigDecimal getTrxAmt() {
		if (cdtDbtInd.equalsIgnoreCase("DBIT"))
		{
			return new BigDecimal(amt).negate();
		}
		if (cdtDbtInd.equalsIgnoreCase("CRDT"))
		{
			return new BigDecimal(amt);
		}
		lastError = "No known CdtDbtInd-Value" ;
		lastErrorDescription = "There was a unknown value for the transaction type in the SEPA file." ;
		return null ;
	}

	@Override
	public BigDecimal getInterestAmt() {
		return new BigDecimal(0.0);
	}

	@Override
	public String getMemo() {
		return ustrd.toString();
	}

	@Override
	public String getChargeName() {
		return null;
	}

	@Override
	public BigDecimal getChargeAmt() {
		return null;
	}

	@Override
	public String getIBAN() {
		return myAcct;
	}

}
